/*
abstract class Event(val window:RenderWindow, val font:Font, val event_name:String) :
	def draw()
	def end_of_event() : Int //if the event costs money, returns its cost, else returns 0
*/

import sfml.graphics.*
import sfml.window.*
import com.github.nscala_time.time.Imports._
import scala.math.*
import scala.util.Random
object Random extends Random


class GreveDesCheminots(trainbuttons : Array[TransportButton[Train]], window : RenderWindow, font : Font) :
	
	var active : Boolean = false
	val covers = new Array[Button](trainbuttons.length)
	var cover_to_display = new Array[Boolean](trainbuttons.length)
	var finish_time : Float = -1f //will be properly initialized when needed

	def is_active() : Boolean =
		active
	
	def is_covered(i:Int) : Boolean =
		cover_to_display(i)

	def setup() =
		printf("Greve setup!\n")
		for (i <- 0 to (covers.length-1)) do
			covers(i) = Button("en greve", Rect(30, 60f + 120f * (i+1), 100, 50), font)
			cover_to_display(i) = false

	def start_event()=
		printf("Greve started !\n")
		active = true
		for i <- 0 to (cover_to_display.length -1) do {
			
			if (Random.between(0,2)<1) then
				cover_to_display(i) = true
			
			//cover_to_display(i) = true 
			//on purpose, for the trial
		}
		finish_time = Random.between(60000f,300000f) //big on purpose for trial
	
	def finish_event() =
		printf("Greve finished !\n")
		active = false
		for i <- 0 to (cover_to_display.length -1) do {
			cover_to_display(i) = false
		}

	def draw(window:RenderWindow, font:Font) =
		val txt = "Greve des cheminots\n"
		var r = 255
		var v = 0
		var b = 0
		if (active) then
			if (finish_time <= 1000f) then
				r = 255
				v = 30
				b = 0
			else 
				r = 255
				v = 127
				b = 0
			printf("drawing Greve !\n")
			val to_display = txt + " : " + f"${finish_time}%.0f" 
			val text = Text(to_display, font, 15)
			text.fillColor = sfml.graphics.Color(r, v, b);
			text.outlineColor = sfml.graphics.Color(0, 0, 0);
			text.outlineThickness = 2
			text.position = (window.size.x - text.localBounds.width - 20f, 60f)
			//text.position = (window.size.x - text.localBounds.width - 100f, 10f)
			window.draw(text)

			for i <- 0 to (cover_to_display.length -1) do {
				if (cover_to_display(i)) then
					printf("covering a train")
					covers(i).draw(window)
			}


	
	def update(dt:Float) : Int =
		finish_time -= dt
		if (active) then
			if (finish_time <= 0) then
				finish_event()
		else if (Random.between(0,1000)<1) then
			start_event()
		0



