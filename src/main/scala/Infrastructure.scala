import sfml.graphics.*
import sfml.window.*
import scala.math.*

abstract class Infrastructure[Tra <: Transport](graph:World, transports: Array[Tra], val window:RenderWindow, val font:Font, val transport_name:String) :
	/* graph represent the accessibility of the different cities in the world in relation to each other */


	var transportbuttons = new Array[TransportButton[Tra]]( transports.length );
	var last_button_pressed : Option[Int] = None;
	var upgrade_button = new Array[Button]( transports.length );
	var price_buy = Array.range(0, transports.length );
	var price_upgrade = new Array[Int]( transports.length );
	var launch_button = Button("Launch", Rect(window.size.x - 120f, window.size.y - 80f, 100f, 60f), font);
	var cancel_button = Button("Cancel", Rect(window.size.x - 240f, window.size.y - 80f, 100f, 60f), font);

	var town_as_int_list : List[Int] = Nil;
	var city_to_int_hashmap = get_city_to_int_hashmap(graph.nodes)

	//Sprite for upgrading :
	val texturearrow = Texture()
	if !(texturearrow.loadFromFile(RESOURCES_DIR + "arrow.png")) then System.exit(1) 
	val spritearrow = Sprite(texturearrow)
	spritearrow.scale = (40f / spritearrow.localBounds.width, 40f / spritearrow.localBounds.width)

	//Colors for sprite of transports
	val blue = sfml.graphics.Color(0, 0, 255)
	val green = sfml.graphics.Color(0, 255, 0)
	val red = sfml.graphics.Color(255, 0, 0)
	val yellow = sfml.graphics.Color(255, 255, 0)
	val purple = sfml.graphics.Color(255, 0, 255)
	val colors = List(blue, green, red, yellow, purple)

	def unlock_transport(i : Int) = 
		if (i < transports.length) 
			transportbuttons(i) = TransportButton[Tra](Some(transports(i)), transports(i).name, Rect(30, 60f + 120f * (i+1), 100, 50), font)
			transports(i).sprite.color = colors(i%5)
			transports(i).unlock_transport() 	 


	def setup() =
		for (i <- 0 to transports.length -1) do
			transportbuttons(i) = TransportButton(None, s"New ${transport_name} (${price_buy(i)}" + "$)", Rect(30, 60f + 120f * (i+1), 100, 50), font)
		for (i <- 0 to upgrade_button.length -1) do
			upgrade_button(i) = Button(s"(${price_upgrade(i)}" + "$)", Rect(30 + 100 + 10, 60f + 120f * (i+1), 40, 50), font)

		
	def int_to_town_list(tl : List[Int]) : List[City] =

		//use built-in functions :
		// tl.sliding(2).map{case List(x,y) => graph.getEdge(x,y)}.flatten
		//fenetre glissante de 2, application de getEdge, puis élimination des None
		tl match {
		case Nil => Nil
		case c0::tll => {		   	
			var res : List[City] = Nil		
			var l = tll
			var current = c0
			printf("current city : %d\n", current)
			var current_city = graph.nodes(current)
			while (l != Nil) {
				val next = l.head
				res = {
					graph.getEdge(current, next) match {
					case None => res
					case Some(_) => current_city :: res;
					}
				}
				current = next
				printf("current city : %d\n", current)
				current_city = graph.nodes(current)
				l = {
					l match {
					case Nil => Nil
					case last_city :: Nil => 
						res = {
							graph.getEdge(current, last_city) match {
							case None => current_city :: res
							case Some(_) =>  graph.nodes(last_city) :: current_city :: res;
							}
						}
						Nil
					case _ :: ll => ll
					}
					}
				}
				res.map{c => printf(c.name + "\n")}
				res
		}
	}
	

	def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int, money:Int, requestHandler:RequestHandler) : Int =

		// Planning a trip :
		for i <- 0 to (graph.nodes.length -1) do
			//if town is clicked and we are planning a trip
			if (graph.nodes(i).mousePressed(b, mouseX, mouseY) && (last_button_pressed != None)) { 
				town_as_int_list match {
					case Nil => town_as_int_list = (i) :: Nil
					case j :: _ => {
						if (graph.areLinked(j, i)) { //TO CHANGE ! Maybe in an override in each subclass.
							town_as_int_list = i :: town_as_int_list
						}
					}
				}
			}
		
		// Launching a trip :
		if (launch_button.mousePressed(b, mouseX, mouseY)) {
				last_button_pressed match {
				case None => ()
				case Some(i) =>  {
					val bu = transportbuttons(i)
					bu.t match {
					case None => ()
					case Some(transport) => 
						if (! transport.moving && (town_as_int_list.length > 1)) {
							val road = int_to_town_list(town_as_int_list.reverse).reverse;
							transport.begin_travel(road)
							requestHandler.transport_launched(transport)
							last_button_pressed = None;
							town_as_int_list = Nil;
						}
					}
				}
			}
		}
		// Cancel a trip : 
		if (cancel_button.mousePressed(b, mouseX, mouseY)) {
			last_button_pressed = None;
			town_as_int_list = Nil;
		}
		0



	def update(dt:Float) : Int =
		var benefit = 0;
		for (t <- transports)
			benefit += t.update(dt);
		benefit

	def draw(show_ui:Boolean) =
		//draw tchoutchous
		for (t <- transports)
			t.draw(window)
		if (show_ui)
			for (bu <- transportbuttons)
				bu.draw(window)
			for (i <- town_as_int_list) {
				val c = graph.nodes(i)
				val circle = sfml.graphics.CircleShape()
				circle.radius = (sizeFactor * c.size).toFloat
				circle.origin = ( circle.radius, circle.radius)
				circle.fillColor = sfml.graphics.Color(255, 0x21, 0x34)
				circle.outlineThickness = 1.2
				circle.position = c.position
				window.draw(circle)
			}
			launch_button.draw(window)
			cancel_button.draw(window)
			
			last_button_pressed match {
				case None => ()
				case Some(i) => {
					val bu = transportbuttons(i)
					bu.t match {
					case None => ()
					case Some(transport) =>
						bu.draw_rectangle(window, bu.box, Color.Red())
						bu.draw_transport(window, transport)
						bu.draw_text_below(window, Text(bu.name, font, 20))
					}
				}
			}
			for (i <- 0 to upgrade_button.length -1) do
				transportbuttons(i).t match {
					case None => () 
					case Some (_) =>
						upgrade_button(i).draw(window)
						upgrade_button(i).draw_rectangle(window, upgrade_button(i).box, Color(255, 0, 255))
						upgrade_button(i).draw_sprite(window, spritearrow)
						upgrade_button(i).draw_text_below(window, Text(upgrade_button(i).name, font, 20))
				}




// ******************************************************** TRAIN **************************************************************
class Infra_Train(graph : World, transports : Array[Train], window : RenderWindow, font : Font, transport_name : String) extends Infrastructure[Train](graph, transports, window, font, transport_name) :
	var build_rails_button = Button("Build rails", Rect(window.size.x - 120f, window.size.y - 160f, 100f, 60f), font);

	var grevedescheminots = GreveDesCheminots(transportbuttons, window, font)

	override def update(dt:Float) =
		super.update(dt)+grevedescheminots.update(dt)

	override def setup() =
		grevedescheminots.setup()
		price_buy = price_buy.map(x => 100 * x)
		price_upgrade = price_upgrade.map(_ => 50)
		super.setup()


	override def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int, money:Int, requestHandler:RequestHandler) : Int =
		var spent_money = 0

		// Building new rails :
		for i <- 0 to (graph.nodes.length -1) do
			//if town is clicked and we are building roads from/to this town
			if (graph.nodes(i).mousePressed(b, mouseX, mouseY) && (last_button_pressed == None)) { 
				town_as_int_list match {
					case Nil => town_as_int_list = (i) :: Nil
					case j :: _ => {
						if (!graph.areLinked(j, i) ) {
							town_as_int_list = i :: town_as_int_list
						}
					}
				}
			}
		
		if ((money >= 500 * (town_as_int_list.length - 1)) && (town_as_int_list.length > 1) && build_rails_button.mousePressed(b, mouseX, mouseY)) {
			town_as_int_list.sliding(2,1).toList.map(towns => {spent_money += 500; addDoubleEdgeW(graph, towns(0), towns(1))});
			last_button_pressed = None;
			town_as_int_list = Nil;
		}

		spent_money + super.mousePressed(b, mouseX, mouseY, money, requestHandler)
		
		// Unlocking transport :
		for i <- 0 to (transportbuttons.length -1) do
			val bu = transportbuttons(i)
			if (bu.mousePressed(b, mouseX, mouseY)) {
				bu.t match {
					case None => //buying a transport
						if ((money >= 100 * i) && (!grevedescheminots.is_covered(i))) {
							spent_money += 100 * i // TODO: fine-tune prices
							unlock_transport(i)
						}
					case Some(t) => //selecting a transport
						if (!grevedescheminots.is_covered(i)) then
							last_button_pressed = Some(i)
							town_as_int_list = city_to_int_hashmap.getOrElse(t.current_city, 0) :: Nil
					}
			}


		// Upgrading transport :
		for (i <- 0 to upgrade_button.length -1) do
			transportbuttons(i).t match {
				case None => () 
				case Some (transport) => //if the transport was bought
					if ( (upgrade_button(i).mousePressed(b, mouseX, mouseY)) && (money >= 50)) {
						spent_money += 50 // TODO: fine-tune prices
						transport.vit += .0075f
						//println(s"Upgraded speed of ${transport.name}")
					}
			}
		
		spent_money


	

	override def draw(show_ui : Boolean) =
		super.draw(show_ui);

		grevedescheminots.draw(window, font)

		if (show_ui) {
			build_rails_button.draw(window);
		}



// ******************************************************** PLANE ***************************************************************
class Infra_Plane(graph : World, transports : Array[Plane], window : RenderWindow, font : Font, transport_name : String) extends Infrastructure[Plane](graph, transports, window, font, transport_name) :

	override def setup() =
		price_buy = price_buy.map(x => 300 * (x+1))
		price_upgrade = price_upgrade.map(_ => 100)
		super.setup()

	override def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int, money:Int, requestHandler:RequestHandler) : Int =
		var spent_money = 0

		super.mousePressed(b, mouseX, mouseY, money, requestHandler)
		
		// Unlocking transport :
		for i <- 0 to (transportbuttons.length -1) do
			val bu = transportbuttons(i)
			if (bu.mousePressed(b, mouseX, mouseY)) {
				bu.t match {
					case None => //buying a transport
						if (money >= 300 * (i+1)) {
							spent_money += 300 * (i+1) // TODO: fine-tune prices
							unlock_transport(i)
						}
					case Some(t) => //selecting a transport
						last_button_pressed = Some(i)
						town_as_int_list = city_to_int_hashmap.getOrElse(t.current_city, 0) :: Nil
					}
			}


		// Upgrading transport :
		for (i <- 0 to upgrade_button.length -1) do
			transportbuttons(i).t match {
				case None => () 
				case Some (transport) => //if the transport was bought
					if ( (upgrade_button(i).mousePressed(b, mouseX, mouseY)) && (money >= 100)) {
						spent_money += 100 // TODO: fine-tune prices
						transport.vit += .0075f
						//println(s"Upgraded speed of ${transport.name}")
					}
			}
		spent_money

// ********************************************************** BUS **************************************************************
class Infra_Bus(var graph : World, transports : Array[Bus], window : RenderWindow, font : Font, transport_name : String) extends Infrastructure[Bus](graph, transports, window, font, transport_name) :
	var build_road_button = Button("Build road", Rect(window.size.x - 120f, window.size.y - 160f, 100f, 60f), font);

	override def setup() =
		price_buy = price_buy.map(x => 50 * (x+1))
		price_upgrade = price_upgrade.map(_ => 25)
		super.setup()

	override def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int, money:Int, requestHandler:RequestHandler) : Int =
		var spent_money = 0

		// Building new rails :
		for i <- 0 to (graph.nodes.length -1) do
			//if town is clicked and we are building roads from/to this town
			if (graph.nodes(i).mousePressed(b, mouseX, mouseY) && (last_button_pressed == None)) { 
				town_as_int_list match {
					case Nil => town_as_int_list = (i) :: Nil
					case j :: _ => {
						if (!graph.areLinked(j, i)) {
							town_as_int_list = i :: town_as_int_list
						}
					}
				}
			}
		
		if ((money >= 200 * (town_as_int_list.length - 1)) && (town_as_int_list.length > 1) && build_road_button.mousePressed(b, mouseX, mouseY)) {
			printf("building a road !\n\n\n")
			town_as_int_list.sliding(2,1).toList.map(towns => {spent_money += 200; addDoubleEdgeW(graph, towns(0), towns(1))});
			last_button_pressed = None;
			town_as_int_list = Nil;
		}

		// Unlocking transport :
		for i <- 0 to (transportbuttons.length -1) do
			val bu = transportbuttons(i)
			if (bu.mousePressed(b, mouseX, mouseY)) {
				bu.t match {
					case None => //buying a transport
						if (money >= 50 * (i+1)) {
							spent_money += 50 * (i+1) // TODO: fine-tune prices
							unlock_transport(i)
						}
					case Some(t) => //selecting a transport
						last_button_pressed = Some(i)
						town_as_int_list = city_to_int_hashmap.getOrElse(t.current_city, 0) :: Nil
					}
			}


		// Upgrading transport :
		for (i <- 0 to upgrade_button.length -1) do
			transportbuttons(i).t match {
				case None => () 
				case Some (transport) => //if the transport was bought
					if ( (upgrade_button(i).mousePressed(b, mouseX, mouseY)) && (money >= 25)) {
						spent_money += 25 // TODO: fine-tune prices
						transport.vit += .0075f
						//println(s"Upgraded speed of ${transport.name}")
					}
			}
		

		// Building a new road :

		spent_money + super.mousePressed(b, mouseX, mouseY, money, requestHandler)

	override def draw(show_ui : Boolean) =
		super.draw(show_ui);

		if (show_ui) {
			build_road_button.draw(window);
		}

// ******************************************************** BOAT **************************************************************
class Infra_Boat(graph : World, transports : Array[Boat], window : RenderWindow, font : Font, transport_name : String) extends Infrastructure[Boat](graph, transports, window, font, transport_name) :

	override def setup() =
		price_buy = price_buy.map(x => 200 * (x+1))
		price_upgrade = price_upgrade.map(_ => 75)
		super.setup()

	override def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int, money:Int, requestHandler:RequestHandler) : Int =
		var spent_money = 0

		super.mousePressed(b, mouseX, mouseY, money, requestHandler)
	
		// Unlocking transport :
		for i <- 0 to (transportbuttons.length -1) do
			val bu = transportbuttons(i)
			if (bu.mousePressed(b, mouseX, mouseY)) {
				bu.t match {
					case None => //buying a transport
						if (money >= 200 * (i+1)) {
							spent_money += 200 * (i+1) // TODO: fine-tune prices
							unlock_transport(i)
						}
					case Some(t) => //selecting a transport
						last_button_pressed = Some(i)
						town_as_int_list = city_to_int_hashmap.getOrElse(t.current_city, 0) :: Nil
					}
			}


		// Upgrading transport :
		for (i <- 0 to upgrade_button.length -1) do
			transportbuttons(i).t match {
				case None => () 
				case Some (transport) => //if the transport was bought
					if ( (upgrade_button(i).mousePressed(b, mouseX, mouseY)) && (money >= 75)) {
						spent_money += 75 // TODO: fine-tune prices
						transport.vit += .0075f
						//println(s"Upgraded speed of ${transport.name}")
					}
			}
		

		spent_money


