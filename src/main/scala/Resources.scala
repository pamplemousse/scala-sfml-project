import sfml.graphics.*
import sfml.window.*

enum resource_type :
    case Wood, Ore, Apple, Coal, Parts, Engine, Iron

class Resource(val res : resource_type) :
	val wood_texture = Texture()
	if !(wood_texture.loadFromFile(RESOURCES_DIR + "resources/wood.png")) then System.exit(1) 
	val wood_sprite = Sprite(wood_texture)
	wood_sprite.scale = (.11f, .11f)
	val ore_texture = Texture()
	if !(ore_texture.loadFromFile(RESOURCES_DIR + "resources/ore.png")) then System.exit(1) 
	val ore_sprite = Sprite(ore_texture)
	ore_sprite.scale = (.15f, .15f)
	val apple_texture = Texture()
	if !(apple_texture.loadFromFile(RESOURCES_DIR + "resources/apple.png")) then System.exit(1) 
	val apple_sprite = Sprite(apple_texture)
	apple_sprite.scale = (.16f, .16f)
	val coal_texture = Texture()
	if !(coal_texture.loadFromFile(RESOURCES_DIR + "resources/coal.png")) then System.exit(1) 
	val coal_sprite = Sprite(coal_texture)
	coal_sprite.scale = (.16f, .16f)
	val parts_texture = Texture()
	if !(parts_texture.loadFromFile(RESOURCES_DIR + "resources/parts.png")) then System.exit(1) 
	val parts_sprite = Sprite(parts_texture)
	parts_sprite.scale = (.11f, .11f)
	val engine_texture = Texture()
	if !(engine_texture.loadFromFile(RESOURCES_DIR + "resources/engine.png")) then System.exit(1) 
	val engine_sprite = Sprite(engine_texture)
	engine_sprite.scale = (.12f, .12f)
	val iron_texture = Texture()
	if !(iron_texture.loadFromFile(RESOURCES_DIR + "resources/iron.png")) then System.exit(1) 
	val iron_sprite = Sprite(iron_texture)
	iron_sprite.scale = (.04f, .04f)

	def draw_resource(window:RenderWindow, font : Font, pos: (Float, Float)) = 
		val sprite = res match {
			case resource_type.Wood  => wood_sprite
			case resource_type.Ore	 => ore_sprite
			case resource_type.Apple => apple_sprite
			case resource_type.Coal  => coal_sprite
			case resource_type.Parts => parts_sprite
			case resource_type.Engine=>engine_sprite
			case resource_type.Iron  => iron_sprite
		}
		sprite.origin = (sprite.localBounds.width/2, sprite.localBounds.height/2)
		sprite.position = pos
		sprite.rotation = 0
		window.draw(sprite)


// TODO : give satisfactory satisfaction condition for resources request (like by checking the cities' demand (this city alone, or compared to other cities on the road))
class ResourcesRequest(val from: City, val res_t: resource_type, val reward: Int) extends SimpleShape with Reward(reward_type.Money, reward) with Request:
	var current_city = from;
	val res = Resource(res_t); //TODO : use a hastbl instead to avoid loading sprites each times
	
	def city_satisfies(city : City) : Boolean = (city != from) && city.resources_demands.contains(res_t)
		
	def is_satisfiable(worldMap: World) : Boolean = true //checks if there is a city than can satisfy the constraints
	def check_failed(transport_option : Option[Transport]) : Boolean = false
	//both functinos are hard coded, but this works with the rest of the code
		
	def check_satisfied(transport_option : Option[Transport]) =
		transport_option match {
			case None => ()
			case Some(transport) => current_city = transport.current_city
		}
		city_satisfies(current_city)

	def update(dt: Float, transport_option : Option[Transport]) =
		if(check_satisfied(transport_option)) then ()
			// TODO : signal win (reward appearing)
		else if (check_failed(transport_option)) then ()
			// TODO : signal fail (constraint not satisfied)
		else ()
		if check_satisfied(transport_option) then {
			//println(s"won ${reward} by selling resources !") 
			reward
		}
		else 0


	def draw (window:RenderWindow, font : Font, offset:Int, transport_option : Option[Transport]) = 
		transport_option match {
			case None => 
				val (x, y) = current_city.position
				val pos = (x, (y - sizeFactor * (offset + current_city.size+1)).toFloat)
				res.draw_resource(window, font, pos)
			case Some(transport) =>
				val (x, y) = transport.position_moving()
				val pos = (x, (y - sizeFactor * (offset + current_city.size+1)).toFloat)
				res.draw_resource(window, font, pos)
		}


def resource_hashmap() : scala.collection.mutable.HashMap[resource_type, Resource] =
	val res = scala.collection.mutable.HashMap[resource_type, Resource]()
	res.update(resource_type.Wood	, Resource(resource_type.Wood	))
	res.update(resource_type.Ore	, Resource(resource_type.Ore	))
	res.update(resource_type.Apple	, Resource(resource_type.Apple	))
	res.update(resource_type.Coal	, Resource(resource_type.Coal	))
	res.update(resource_type.Engine	, Resource(resource_type.Engine	))
	res.update(resource_type.Parts	, Resource(resource_type.Parts	))
	res.update(resource_type.Iron	, Resource(resource_type.Iron	))
	res
//TODO : is there a better alternative to Lists for this use case ?
val resource_sprite_size = 45f
class Industry(val name : String, val inputs : List[resource_type], val outputs : List[resource_type]) extends SimpleShape :
	val sprite_hashmap = resource_hashmap()

	def draw_text_left(window:RenderWindow, tf : sfml.graphics.Text, pos: (Float, Float)) =
		tf.fillColor = sfml.graphics.Color(255, 100, 255);
		tf.outlineColor = sfml.graphics.Color(0, 0, 0);
		tf.outlineThickness = 3
		tf.origin = (tf.localBounds.width/2, tf.localBounds.height/2)
		tf.position = pos
		window.draw(tf)
		
	def draw (window:RenderWindow, font : Font, x : Float, y : Float) = 
		var offset:Float = 0
		//draws the input
		for (q <- inputs)
			sprite_hashmap.get(q).get.draw_resource(window, font, (x + offset * resource_sprite_size, y))
			offset = offset + 1
		if (inputs == Nil)
			draw_circle(window, sfml.graphics.Color.Transparent(), (x + offset * resource_sprite_size, y), 1)
			offset = offset + 1

		draw_text_left(window, Text("->", font, 20), (x + offset * resource_sprite_size, y))
		offset = offset + 1

		//draws the output
		for (q <- outputs)
			sprite_hashmap.get(q).get.draw_resource(window, font, (x + offset * resource_sprite_size, y))
			offset = offset + 1
		if (outputs == Nil)
			draw_circle(window, sfml.graphics.Color.Transparent(), (x + offset * resource_sprite_size, y), 1)
			offset = offset + 1

		offset = offset + 0.5f
		draw_text_left(window, Text("(" + name + ")", font, 20), (x + offset * resource_sprite_size, y))

		