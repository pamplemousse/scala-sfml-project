import sfml.graphics.*
import sfml.window.*
import com.github.nscala_time.time.Imports._
import scala.math.*

import java.io.File 
import java.io.PrintWriter 
import scala.io.Source  
import scala.util.Try

val RESOURCES_DIR = "src/main/resources/"

//TODO :
//def int_to_town_list 
	//use built-in functions :
	// tl.sliding(2).map{case List(x,y) => train_graph.getEdge(x,y)}.flatten
	//fenetre glissante de 2, application de getEdge, puis élimination des None

enum infrastructure_type :
	case Train_type, Plane_type, Bus_type, Boat_type

class Game(val window:RenderWindow, val font:Font) :

	val texture_train = Texture();
	if !(texture_train.loadFromFile(RESOURCES_DIR + "Train.png")) then System.exit(1);
	val texture_plane = Texture();
	if !(texture_plane.loadFromFile(RESOURCES_DIR + "avion_helice.png")) then System.exit(1);
	val texture_bus = Texture();
	if !(texture_bus.loadFromFile(RESOURCES_DIR + "bus.png")) then System.exit(1);
	val texture_boat = Texture();
	if !(texture_boat.loadFromFile(RESOURCES_DIR + "boat.png")) then System.exit(1);


	var placeholder_sea_rectangle = sfml.graphics.RectangleShape();
	placeholder_sea_rectangle.position = (1300f, -100f);
	placeholder_sea_rectangle.size = (window.size.x * 1f, window.size.y * 2f);
	placeholder_sea_rectangle.fillColor = sfml.graphics.Color(0, 105, 148);
	placeholder_sea_rectangle.rotation = 1.5f;

	
	var train_graph = smallerDrawableGraph(font)  
	var trains : Array[Train] = Array( 
					Train("Old Freighter"	, .02f, train_graph.nodes(0), Sprite(texture_train), font),
					Train("Frog Train"		, .04f, train_graph.nodes(0), Sprite(texture_train), font),  
					Train("Bunny Train"		, .06f, train_graph.nodes(0), Sprite(texture_train), font), 
					Train("Polar Express"	, .08f, train_graph.nodes(0), Sprite(texture_train), font),  
					Train("Turbo Loco"		, .10f, train_graph.nodes(0), Sprite(texture_train), font)); 
	var train_infrastructure = Infra_Train(train_graph, trains, window, font, "train");
	var bus_graph = bus_world(train_graph)
	var buses : Array[Bus] = Array( 
					Bus("Bus Pomme"		, .01f, train_graph.nodes(3), Sprite(texture_bus), font),
					Bus("Bus Poire"		, .02f, train_graph.nodes(3), Sprite(texture_bus), font),  
					Bus("Bus Clementine", .03f, train_graph.nodes(3), Sprite(texture_bus), font), 
					Bus("Bus Cerise"	, .04f, train_graph.nodes(3), Sprite(texture_bus), font),  
					Bus("Bus Kumkwat"	, .05f, train_graph.nodes(3), Sprite(texture_bus), font)); 
	var bus_infrastructure = Infra_Bus(bus_graph, buses, window, font, "bus");
	var planes : Array[Plane] = Array( 
					Plane("Colibri Cendré"	, .07f, train_graph.nodes(1), Sprite(texture_plane), font),	
					Plane("Aigle Enneigé"	, .07f, train_graph.nodes(1), Sprite(texture_plane), font),	
					Plane("Pigeon d'Argent"	, .07f, train_graph.nodes(1), Sprite(texture_plane), font),	
					Plane("Flammant Violet"	, .07f, train_graph.nodes(1), Sprite(texture_plane), font),	
					Plane("Perroquet d'Or"	, .07f, train_graph.nodes(1), Sprite(texture_plane), font)	); 
	var plane_infrastructure = Infra_Plane(fully_connected_world(train_graph), planes, window, font, "plane");
	var boats : Array[Boat] = Array( 
					Boat("Macquereau"		, .05f, train_graph.nodes(10), Sprite(texture_boat), font),	
					Boat("Carpe"			, .06f, train_graph.nodes(8 ), Sprite(texture_boat), font),	
					Boat("Poisson Chat"		, .07f, train_graph.nodes(4 ), Sprite(texture_boat), font),	
					Boat("Bar"				, .08f, train_graph.nodes(10), Sprite(texture_boat), font),	
					Boat("Saumon"			, .09f, train_graph.nodes(8 ), Sprite(texture_boat), font)	); 
	var boat_infrastructure = Infra_Boat(boat_world(train_graph), boats, window, font, "boat");
	
	var current_infrastructure:infrastructure_type = infrastructure_type.Train_type
	var infrastructure_button = Button(">", Rect(55, 60f, 50, 50), font);

	var save_button = Button("Save", Rect(30, 740, 100, 50), font);

	var requestHandler : RequestHandler = RequestHandler(train_graph, font)

	
	//need to be able to add/suppress elements
	var money : Int = 150;
	
	def setup() =
		train_infrastructure.setup()
		train_infrastructure.unlock_transport(0)
		bus_infrastructure.setup()
		plane_infrastructure.setup()
		boat_infrastructure.setup()

	
	def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) = 
		train_graph.mousePressed(b, mouseX, mouseY)
		if (infrastructure_button.mousePressed(b, mouseX, mouseY))
			current_infrastructure = {
				current_infrastructure match {
					case infrastructure_type.Train_type => infrastructure_type.Plane_type 
					case infrastructure_type.Plane_type => infrastructure_type.Bus_type 
					case infrastructure_type.Bus_type	=> infrastructure_type.Boat_type 
					case infrastructure_type.Boat_type	=> infrastructure_type.Train_type 
				}
			}
		current_infrastructure match {
			case infrastructure_type.Train_type =>	money = money - train_infrastructure.mousePressed(b, mouseX, mouseY, money, requestHandler)
			case infrastructure_type.Plane_type =>	money = money - plane_infrastructure.mousePressed(b, mouseX, mouseY, money, requestHandler)
			case infrastructure_type.Bus_type	=>	money = money - bus_infrastructure.mousePressed(b, mouseX, mouseY, money, requestHandler)
			case infrastructure_type.Boat_type	=>	money = money - boat_infrastructure.mousePressed(b, mouseX, mouseY, money, requestHandler)
		}
		if (save_button.mousePressed(b, mouseX, mouseY))
			save("saves/GAMESAVE.txt")

	def update(dt:Float) =
		var benefit : Int = 0;
		benefit += train_infrastructure.update(dt);
		benefit += plane_infrastructure.update(dt);
		benefit += bus_infrastructure.update(dt);
		benefit += boat_infrastructure.update(dt);
		benefit += requestHandler.update(dt);
		money += benefit

	def display_money(window: RenderWindow) =
		val to_display = s"$money" + " $"
		val text = Text(to_display, font, 20)
		text.fillColor = sfml.graphics.Color(200, 200, 0);
		text.outlineColor = sfml.graphics.Color(0, 0, 0);
		text.outlineThickness = 2
		//text.origin = ( (rect.left - rect.width) / 2, (rect.top - rect.height) / 2)
		text.position = (window.size.x - text.localBounds.width - 10f, 20f)
		window.draw(text)

	var test_industry1 = Industry("test", resource_type.Wood :: Nil, resource_type.Ore :: resource_type.Apple :: Nil)
	var test_industry2 = Industry("test1", Nil, resource_type.Ore :: resource_type.Apple :: Nil)
	var test_industry3 = Industry("test2", resource_type.Wood :: Nil,  Nil)

	def draw() = //le rendering
		window.clear()
		window.draw(placeholder_sea_rectangle)
		bus_graph.draw(window)
		train_graph.draw(window)
		infrastructure_button.draw(window)
		save_button.draw(window)
		current_infrastructure match {
			case infrastructure_type.Train_type => {
				train_infrastructure.draw(true )
				plane_infrastructure.draw(false)
				bus_infrastructure.draw(  false)
				boat_infrastructure.draw( false)
			}
			case infrastructure_type.Plane_type => {
				train_infrastructure.draw(false)
				plane_infrastructure.draw(true )
				bus_infrastructure.draw(  false)
				boat_infrastructure.draw( false)
			}
			case infrastructure_type.Bus_type	=> {
				train_infrastructure.draw(false)
				plane_infrastructure.draw(false)
				bus_infrastructure.draw(  true )
				boat_infrastructure.draw( false)
			}
			case infrastructure_type.Boat_type	=> {
				train_infrastructure.draw(false)
				plane_infrastructure.draw(false)
				bus_infrastructure.draw(  false)
				boat_infrastructure.draw( true )
			}
		} 
		/*
		test_industry1.draw(window, font, 750, 650f)
		test_industry2.draw(window, font, 750, 700f)
		test_industry3.draw(window, font, 750, 750f)
		*/
		requestHandler.draw(window)
		display_money(window)
		/* Update the window */
		window.display()

	val infrastructure_list = List(train_infrastructure, plane_infrastructure, bus_infrastructure, boat_infrastructure)
	def save(fname : String) =
		val file_Object = new File(fname)   
		val print_Writer = new PrintWriter(file_Object)  
		print_Writer.write(s"${money}\n")  
		for(infra<-infrastructure_list.toIterator) {
			print_Writer.write(s"${infra.transport_name}\n")  
			for (i <- 0 to infra.transportbuttons.length -1)
				infra.transportbuttons(i).t match {
				case None => {}
				case Some(transport) => {
					print_Writer.write(s"${i}\n")  	
					print_Writer.write(s"${transport.vit}\n")  					
					}
				}
		}
		print_Writer.write("End\n")  
		print_Writer.close() 

	def load(fname : String) =
		if (new java.io.File(fname).exists)
			/*
			val fSource = Source.fromFile(fname)  
			for(line<-fSource.getLines) {  
				println(line)  
			}  
			*/
			val fSource = Source.fromFile(fname) 
			val iter = fSource.getLines
			money = iter.next().toInt
			iter.next()
			for(infra<-infrastructure_list.toIterator) {
				var done = false
				while (! done)
					Try(iter.next().toInt).toOption match {
					case None => {done = true} //must be an identifier : the next infrastructure's name, or "End"
					case Some(i) => {
						var v = iter.next().toFloat
						infra.unlock_transport(i)
						infra.transportbuttons(i).t.get.vit = v						
					}
				}
			}
			fSource.close()  




