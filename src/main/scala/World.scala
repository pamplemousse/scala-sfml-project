import sfml.graphics.*
import sfml.window.*
import com.github.nscala_time.time.Imports._
import scala.math.*

enum city_type :
	case Circle, Triangle, Square
	
val maxCitySize = 4

class City(name: String, size: Int, position: (Float, Float), val ctype : city_type) extends DrawableNumNode(name, size, position) :
//need to find a suitable factor pop (City) -> size (DrawableNumNode)

	var pop = size*100;
	var pop_in_station = max(10, pop/100);
	var ticket_fare = 1;
	var industries : List[Industry] = Nil;
	var resources_demands : List[resource_type] = Nil;
	var resources_production : List[resource_type] = Nil;

	def update_industries() =
		//ces fonctions lisent les inputs/outputs des industries, et les concatène.
		resources_demands =		(industries.map(i => i.inputs )).fold(Nil)((accumulateur, l) => l ::: accumulateur)
		resources_production =	(industries.map(i => i.outputs)).fold(Nil)((accumulateur, l) => l ::: accumulateur)

	def get_pop_waiting() : Int = pop_in_station; 

	def departure(people: Int) = //function special to accomodate future merchandises/other palipipous
	//test débordement ? possible ?
		pop_in_station = max(10, pop_in_station - people);
	// if (_pop < passengers) then Failure() -> uncomment line 4

	def arrival(people: Int) : Int =
		pop += people;
		pop_in_station = max(10,pop/10);
		people * ticket_fare
		


	/*val font:Font;
	val _name = Text(name, font, 50);*/
	//need to think : global font for the whole "World" ? Does a City take a "font" as parameter ?
	//see name + pop + merchandise ?? + people waiting to depart ??

	
	override def draw(window:RenderWindow) =
		ctype match {
			//TODO : move display of city name here + display city size
			case city_type.Circle => draw_circle(window, sfml.graphics.Color(0x2F, 0x21, 0x34), position, size);
			case city_type.Triangle => draw_triangle(window, sfml.graphics.Color(0x2F, 0x21, 0x34), position, size);
			case city_type.Square => draw_square(window, sfml.graphics.Color(0x2F, 0x21, 0x34), position, size);
		}
		
	override def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) =
		val (x, y) = position
		val dsquared = (x - mouseX) * ( x - mouseX ) + (y - mouseY) * (y - mouseY)
		if (dsquared <= (size) * (size) * sizeFactor * sizeFactor)
			true
		else
			false


class Railtracks(val from: City, val to: City, indexTo: Int, val length: Float) extends Edge(indexTo) :
	var capacity = 1; //may be taken as parameter of Railtracks in the future

	def get_capacity() : Int = capacity;

	def get_from() : City = from;
	def get_to() : City = to;

	def get_length() : Float = length;

	def departure() =
		capacity = capacity - 1;

	def arrival() =
		capacity = capacity + 1;



class World(nodes: Array[City], successors: Array[List[Railtracks]], _font : Font, edge_color: sfml.graphics.Color, edge_size:Float) 
	extends DrawableGraph[City, Railtracks](nodes, successors, edge_color, edge_size) :
	val font:Font = _font;
	var last_clicked : Option[Int] = None

	override def draw (window:RenderWindow) =
		for i <- 0 to (nodes.length -1) do
			(successors(i).map(e => drawLine(window, nodes(i).position, nodes(e.indexTo).position, edge_color, edge_size)))
		for i <- 0 to (nodes.length -1) do
			nodes(i).draw(window)
			val name = Text(nodes(i).name, font, 20)
			val (x, y) = nodes(i).position
			name.fillColor = sfml.graphics.Color(255, 100, 255);
			name.outlineColor = sfml.graphics.Color(0, 0, 0);
			name.outlineThickness = 3
			name.origin = ( name.localBounds.width / 2, name.localBounds.height / 2)
			name.position = (x, y)
			name.rotate(20)
			window.draw(name)
		last_clicked match {
			case None => ();
			case Some(i) => 
				var count = 0
				for ind <- nodes(i).industries do
					ind.draw(window, font, 750, 650f + count * 50f)
					count = count + 1

		} 

	override def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) =
		var res = false
		for i <- 0 to (nodes.length -1) do
			if nodes(i).mousePressed(b, mouseX, mouseY) then
				res = true
				last_clicked = Some(i)
		res


