import sfml.graphics.*
import sfml.window.*


trait Node(val name: String)
class Edge(val indexTo : Int)

class Graph[N <: Node, E <: Edge](val nodes: Array[N], val successors: Array[List[E]]):
	def areLinked(i: Int, j: Int) : Boolean =
		for (s <- successors(i)) do
			if (j == s.indexTo) return true;
		false
	def getEdge(i: Int, j: Int) : Option[E] =
		for (s <- successors(i)) do
			if (j == s.indexTo) return Some(s);
		None
		

class DrawableNumNode(name: String, var size: Int, val position: (Float, Float)) extends SimpleShape with Node(name) with Clickable:
	def draw (window:RenderWindow) =
		val circle = sfml.graphics.CircleShape()
		circle.radius = (sizeFactor * size).toFloat
		circle.origin = ( circle.radius, circle.radius)
		circle.fillColor = sfml.graphics.Color(0x2F, 0x21, 0x34)
		circle.outlineThickness = 1.2
		circle.position = position
		window.draw(circle)
	def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) =
		val (x, y) = position
		val dsquared = (x - mouseX) * ( x - mouseX ) + (y - mouseY) * (y - mouseY)
		if (dsquared <= size * size * sizeFactor * sizeFactor)
			size = 1 + (size %4)
			true
		else
			false

def drawLine(window:RenderWindow, p1: (Float, Float), p2: (Float, Float), c:sfml.graphics.Color, size:Float) =
	val line = RectangleShape()
	line.size = ( math.sqrt((p1(0)-p2(0))*(p1(0)-p2(0)) + (p1(1)-p2(1))*(p1(1)-p2(1))).toFloat, 2f * size)
	line.origin = (2f, 0f)
	line.rotation = (180*math.atan2(p1(1)-p2(1), p1(0)-p2(0))/math.Pi).toFloat
	line.position = p2
	line.fillColor = c
	window.draw(line)
	
class DrawableGraph[N <: DrawableNumNode,  E <: Edge]( nodes: Array[N], successors: Array[List[E]], val edge_color: sfml.graphics.Color, val edge_size: Float) extends Graph[N, E](nodes, successors)  with Clickable :
	def draw (window:RenderWindow) =
		for i <- 0 to (nodes.length -1) do
			(successors(i).map(e => drawLine(window, nodes(i).position, nodes(e.indexTo).position, edge_color, edge_size)))
		for i <- 0 to (nodes.length -1) do
			nodes(i).draw(window)
	def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) =
		var res = false
		for i <- 0 to (nodes.length -1) do
			res = res || nodes(i).mousePressed(b, mouseX, mouseY)
		res

def addEdge(g:DrawableGraph[DrawableNumNode, Edge], n1:Int, n2: Int) =
	g.successors.update(n1, (Edge(n2) :: g.successors(n1)))
def addDoubleEdge(g:DrawableGraph[DrawableNumNode, Edge], n1:Int, n2: Int) =
	addEdge(g, n1, n2)
	addEdge(g, n2, n1)


def addEdgeW(g:World, n1:Int, n2: Int) =
	g.successors.update(n1, (Railtracks(g.nodes(n1), g.nodes(n2), n2, 100) :: g.successors(n1))) //fixed length
def addDoubleEdgeW(g:World, n1:Int, n2: Int) =
	addEdgeW(g, n1, n2)
	addEdgeW(g, n2, n1)


def nameOurCities(i:Int): String=
	val cityNames = Array("Palipipou", "Alibaba", "New York", "Porkytown", "New Louisiana", "Babayaga", "Sparkle Sparkle", "Twinkle Twinkle", "Avocadodo", "Mimosa", "Old York", "Tall People Town", "Short People Town", "Pumpkin Pie", "Issila", "Pink Panther", "Doodoo doodoo", "Eepie Sleepy", "Claquette-Chaussette", "Karnage Town", "Schwoonopolis", "Guilli-guilli", "Disneyland", "Topinambour", "Butternut", "Palipipoupolis", "Gotham City", "Megamind's Heaven", "The Traveller's Inn", "Gloomy creepy town", "Coupe-gorge", "Do not trespass", "Walk away", "Goblin Land", "Paladin's Forteress", "Ghost Town", "New Beginning", "Hope", "Glory", "Idealopolis", "FreeWay", "Totopolis", "Tatapolis", "Concrete Jungle", "Dream", "Rennes", "Toulouse", "Plymouth");
  
	cityNames(i % (cityNames.length))

def cityNumToType(i:Int): city_type =
	val v = scala.math.sqrt( i * 1664525 + 1013904223).toInt //lowcost hash function
	(v%3) match {
		case 0 => city_type.Circle
		case 1 => city_type.Triangle
		case _ => city_type.Square
	}

val industry_list = Array(
	Industry("Population", resource_type.Apple :: Nil, 
		Nil),
	Industry("Farming", Nil, 
		resource_type.Apple :: Nil),
	Industry("Lumber", Nil, 
		resource_type.Wood :: Nil),
	Industry("Agroforestry", resource_type.Engine :: Nil, 
		resource_type.Apple :: resource_type.Wood :: Nil),
	Industry("Coal mining", resource_type.Wood :: Nil, 
		resource_type.Coal :: Nil),
	Industry("Ore mining", resource_type.Wood :: Nil, 
		resource_type.Ore :: Nil),
	Industry("Advanced mining", resource_type.Wood :: resource_type.Engine :: Nil, 
		resource_type.Coal :: resource_type.Ore :: Nil),
	Industry("Wood smelter", resource_type.Wood :: resource_type.Coal :: Nil, 
		resource_type.Coal :: resource_type.Coal :: Nil),
	Industry("Smelter", resource_type.Ore :: resource_type.Coal :: Nil, 
		resource_type.Iron :: Nil),
	Industry("Parts Factory", resource_type.Iron :: resource_type.Iron :: Nil, 
		resource_type.Parts :: Nil),
	Industry("Machinery Factory", resource_type.Iron :: resource_type.Parts :: Nil, 
		resource_type.Engine :: Nil),
	Industry("Assembly Line", resource_type.Wood :: resource_type.Parts :: resource_type.Parts :: resource_type.Engine :: Nil, 
		Nil),
)

def smallerDrawableGraph(/*x0: Float, y0: Float, size: Float,*/ font: Font): World =
	val nodes = Array.ofDim[City](11)
	//Totopolis : 0
	var pos = (680.0f, 597.3f)
	nodes.update(0, City("Totopolis", 4, pos, city_type.Triangle))

	//Topinambour : 1
	pos = (800.0f, 354.286f)
	nodes.update(1, City("Topinambour", 2, pos, city_type.Triangle))

	//Gotham City : 2
	pos = (1160.0f, 354.286f)
	nodes.update(2, City("Gotham City", 2, pos, city_type.Triangle))

	//Schwoonopolis : 3
	pos = (560.0f, 114.286f)
	nodes.update(3, City("Schwoonpolis", 3, pos, city_type.Circle))

	//Toulouse : 4
	pos = (1280.0f, 594.286f)
	nodes.update(4, City("Toulouse", 2, pos, city_type.Square))

	//Rennes : 5
	pos = (1000.0f, 500.7f)
	nodes.update(5, City("Rennes", 1, pos, city_type.Circle))

	//Karnage Town : 6
	pos = (350.0f, 354.286f)
	nodes.update(6, City("Karnage Town", 1, pos, city_type.Circle))

	//Sparkle Sparkle : 7
	pos = (900.0f, 160.31f)
	nodes.update(7, City("Sparkle Sparkle", 3, pos, city_type.Square))

	//Twinkle Twinkle : 8
	pos = (1300.0f, 114.286f)
	nodes.update(8, City("Twinkle Twinkle", 2, pos, city_type.Triangle))

	//Palipipoupolis : 9
	pos = (333.0f, 592.7f)
	nodes.update(9, City("Palipipoupolis", 1, pos, city_type.Square))

	//Neo Atlantis : 10
	pos = (1500.0f, 400f)
	nodes.update(10, City("Neo Atlantis", 2, pos, city_type.Circle))

	for i <- 0 to 10 do
		nodes(i).industries = industry_list(0) :: industry_list(i+1) :: Nil //TODO : add diversity by hand
		nodes(i).update_industries();

	val successors = Array.ofDim[List[Railtracks]](11)
	for a <- 0 to 10 do
		successors.update(a, Nil)
	val g = World(nodes, successors, font, sfml.graphics.Color(0x00, 0xFF, 0x34, 0xe0), 2f)


	//Rennes 5 - Toulouse 4
	addDoubleEdgeW(g, 5, 4)
	//Toulouse 4 - Gotham City 2
	addDoubleEdgeW(g, 4, 2)
	//Toulouse 4 - Twinkle Twinkle 8
	addDoubleEdgeW(g, 4, 8)
	//Rennes 5 - Gotham City 2
	addDoubleEdgeW(g, 5, 2)
	//Rennes 5 - Sparkle Sparkle 7
	addDoubleEdgeW(g, 5, 7)
	//Twinkle Twinkle 8 - Sparkle Sparkle 7
	addDoubleEdgeW(g, 8, 7)
	//Gotham City 2 - Topinambour 1
	addDoubleEdgeW(g, 2, 1)
	//Topinambour 1 - Schwoonopolis 3
	addDoubleEdgeW(g, 1, 3)
	//Schwoonopolis 3 - Karnage Town 6
	addDoubleEdgeW(g, 3, 6)
	//Karnage Town 6 - Palipipoupolis 9
	addDoubleEdgeW(g, 6, 9)
	//Karnage Town 6 - Totopolis 0
	addDoubleEdgeW(g, 6, 0)
	//Palipipoupolis 9 - Totopolis 0
	addDoubleEdgeW(g, 9, 0)
	//Totopolis 0 - Toulouse 4
	addDoubleEdgeW(g, 0, 4)

	//return :
	g

def get_city_to_int_hashmap (nodes : Array[City]) : scala.collection.mutable.HashMap[City, Int] =
	val res = scala.collection.mutable.HashMap[City, Int]()
	val n = nodes.length
	for i <- 0 to (n-1) do
		res.update(nodes(i), i)
	res

def fully_connected_world(graph: World) : World =
	//generate a copy of the given World where every city is connected (= the map that planes use)
	val n = graph.nodes.length
	val successors = Array.ofDim[List[Railtracks]](n)
	for a <- 0 to (n-1) do
		successors.update(a, Nil)
	val g = World(graph.nodes, successors, graph.font, graph.edge_color, graph.edge_size)
	for i <- 0 to (n-1) do
		for j <- 0 to (n-1) do
			addDoubleEdgeW(g, i, j)
	g

def bus_world(graph: World) : World =
	//generate a copy of the given World with a few roads
	val n = graph.nodes.length
	val successors = Array.ofDim[List[Railtracks]](n)
	for a <- 0 to (n-1) do
		successors.update(a, Nil)
	val g = World(graph.nodes, successors, graph.font, sfml.graphics.Color(0xFF, 0x00, 0x34, 0xe0), graph.edge_size *2)

	addDoubleEdgeW(g, 5, 4)
	addDoubleEdgeW(g, 4, 2)
	addDoubleEdgeW(g, 4, 8)
	addDoubleEdgeW(g, 5, 2)
	addDoubleEdgeW(g, 5, 7)
	addDoubleEdgeW(g, 8, 7)
	addDoubleEdgeW(g, 1, 3)
	addDoubleEdgeW(g, 3, 6)
	addDoubleEdgeW(g, 6, 9)
	addDoubleEdgeW(g, 6, 0)
	addDoubleEdgeW(g, 9, 0)
	addDoubleEdgeW(g, 0, 4)
	addDoubleEdgeW(g, 2, 1)
	addDoubleEdgeW(g, 5, 1)
	addDoubleEdgeW(g, 7, 1)
	addDoubleEdgeW(g, 5, 2)
	addDoubleEdgeW(g, 7, 2)
	addDoubleEdgeW(g, 5, 7)
	addDoubleEdgeW(g, 3, 7)
	addDoubleEdgeW(g, 2, 8)
	g

def boat_world(graph: World) : World =
	val n = graph.nodes.length
	val successors = Array.ofDim[List[Railtracks]](n)
	for a <- 0 to (n-1) do
		successors.update(a, Nil)
	val g = World(graph.nodes, successors, graph.font, sfml.graphics.Color(0xFF, 0x00, 0xFF, 0xc0), graph.edge_size *3)

	addDoubleEdgeW(g, 10, 8)
	addDoubleEdgeW(g, 10, 4)
	addDoubleEdgeW(g, 8, 4)
	g