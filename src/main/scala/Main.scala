import scala.util.Using

import sfml.graphics.*
import sfml.window.*

@main def main =
	Using.Manager { use =>
	/* Create the main window */
	val window = use(RenderWindow(VideoMode(1600, 800), "Tchoutchou Corp."))
	window.framerateLimit = 40 //borne supérieure du framerate
	
	/* Create a graphical text to display */
	val font = use(Font())
	if !(font.loadFromFile(RESOURCES_DIR + "FiraSans.ttf")) then System.exit(1)

	val game = Game(window, font)
	game.setup()
	game.load("saves/GAMESAVE.txt")

	val frameGoal = 1000f / 30f //borne inférieure du framerate
	val deltaClock = sfml.system.Clock()
	var paused = false
	/* Start the game loop */
	while window.isOpen() do
		/* Process events */
		for event <- window.pollEvent() do
		event match
			case Event.Closed()               => window.close()
			case Event.Resized(width, height) => window.view = View((0, 0, width, height))
			case Event.MouseButtonPressed(button, x, y)		=> game.mousePressed(button, x, y)
			case Event.KeyPressed(Keyboard.Key.KeySpace, _, _, _, _) 	=> paused = !paused
			case _                            => ()
		var dt = deltaClock.restart().asMilliseconds.toFloat
		if (dt < frameGoal) 
			Thread.sleep((frameGoal - dt).toLong)
			dt = frameGoal
			val _ = deltaClock.restart()
		if (!paused) game.update(dt)	
		//println(s"dt (ms) = ${dt} (goal = ${frameGoal})")
		game.draw()
		

	}.get

	System.exit(0)
