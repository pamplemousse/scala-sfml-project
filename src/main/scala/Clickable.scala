import sfml.graphics.*
import sfml.window.*

	
val sizeFactor:Float = 15f

abstract class SimpleShape: //"Shape" was already taken by SFML
	def draw_circle (window:RenderWindow, color: sfml.graphics.Color, position: (Float, Float), size: Int) =
		val circle = sfml.graphics.CircleShape()
		circle.radius = (sizeFactor * size).toFloat
		circle.origin = ( circle.radius, circle.radius)
		circle.fillColor = color 
		circle.outlineThickness = 1.2
		circle.position = position
		window.draw(circle)
		
	def draw_square (window:RenderWindow, color: sfml.graphics.Color, position: (Float, Float), size: Int) =
		val rectangle = sfml.graphics.RectangleShape()
		rectangle.size = (size * sizeFactor * 1.5, size * sizeFactor * 1.5)
		rectangle.origin = (size * sizeFactor * .75, size * sizeFactor * .75)
		rectangle.fillColor = color
		rectangle.outlineThickness = 1.2
		rectangle.position = position
		window.draw(rectangle)
		
		
	def draw_triangle (window:RenderWindow, color: sfml.graphics.Color, position: (Float, Float), size: Int) =
		val circle = sfml.graphics.CircleShape((sizeFactor * size * 1.2).toFloat, 3)
		circle.origin = ( circle.radius, circle.radius)
		circle.fillColor = color
		circle.outlineThickness = 1.2
		circle.position = position
		window.draw(circle)

trait Clickable:
	def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) : Boolean
	def draw (window: sfml.graphics.RenderWindow) : Unit
	
class Button(val name: String, val box : sfml.graphics.Rect[Float], font: Font) extends Clickable:
	def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) : Boolean =
		if ( (box.left <= mouseX) && (mouseX <= box.left + box.width) && (box.top <= mouseY) && ( mouseY <= box.top + box.height))
			true
		else
			false
	
	def draw_text_center(window:RenderWindow, tf : sfml.graphics.Text) =
		tf.fillColor = sfml.graphics.Color(255, 100, 255);
		tf.outlineColor = sfml.graphics.Color(0, 0, 0);
		tf.outlineThickness = 3
		tf.origin = (tf.localBounds.width/2, tf.localBounds.height/2)
		tf.position = (box.left + (box.width / 2) , box.top + (box.height / 2))
		window.draw(tf)
		
		
	def draw_text_below(window:RenderWindow, tf : sfml.graphics.Text) =
		tf.fillColor = sfml.graphics.Color(60, 160, 255);
		tf.outlineColor = sfml.graphics.Color(0, 0, 0);
		tf.outlineThickness = 3
		tf.origin = (tf.localBounds.width/2, tf.localBounds.height/2)
		tf.position = (box.left + (box.width / 2), box.top + (box.height))
		window.draw(tf)
		
	def draw_sprite(window:RenderWindow, sprite : sfml.graphics.Sprite) =
		sprite.origin = (sprite.localBounds.width/2, sprite.localBounds.height/2)
		sprite.position = (box.left + box.width/2, box.top + box.height/2 )
		sprite.rotation = 0
		window.draw(sprite)
	
	def draw_rectangle (window:RenderWindow, rect : sfml.graphics.Rect[Float], color : sfml.graphics.Color) =
		val rectangle = sfml.graphics.RectangleShape()
		rectangle.size = (rect.width, rect.height)
		rectangle.fillColor = color
		rectangle.outlineThickness = 1.2
		rectangle.position = (rect.left, rect.top)
		window.draw(rectangle)
		
	def draw (window:RenderWindow) =
		draw_rectangle(window, box, sfml.graphics.Color(50, 250, 250))
		draw_text_center(window, Text(name, font, 20))
		


class TransportButton[T <: Transport](val t : Option[T], name: String, box : sfml.graphics.Rect[Float], font: Font) extends Button(name, box, font):

	val textureplus = Texture()
	if !(textureplus.loadFromFile(RESOURCES_DIR + "plus.png")) then System.exit(1) 
	val spriteplus = Sprite(textureplus)
	spriteplus.scale = (60f / spriteplus.localBounds.width, 60f / spriteplus.localBounds.width)
	
	override def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) =
		if ( (box.left <= mouseX) && (mouseX <= box.left + box.width) && (box.top <= mouseY) && ( mouseY <= box.top + box.height))
			true
		else
			false
		
	def draw_transport(window:RenderWindow, transport : T) =
		val sprite_transport : sfml.graphics.Sprite = transport.sprite
		sprite_transport.origin = (sprite_transport.localBounds.width/2, sprite_transport.localBounds.height/2)
		sprite_transport.position = (box.left + box.width/2, box.top + box.height/2 )
		sprite_transport.rotation = 0
		sprite_transport.scale = (transport.spritescale, transport.spritescale)
		window.draw(sprite_transport)
	
	override def draw(window:RenderWindow) =
		t match {
			case None =>
				draw_rectangle(window, box, sfml.graphics.Color(50, 250, 250))
				draw_sprite(window, spriteplus) //TODO add + sprite
				draw_text_below(window, Text(name, font, 20))
			case Some(transport) =>
				draw_rectangle(window, box, sfml.graphics.Color(50, 250, 250))
				draw_transport(window, transport)
				draw_text_below(window, Text(name, font, 20))
		}
		
