import sfml.graphics.*
import sfml.window.*
import scala.math.*


abstract class Transport(val name: String, var vit_init: Float, val spawn_point : City, val sprite: Sprite, val spritescale: Float, val font:Font) extends Clickable:
	var vit = vit_init;
	var capacity_passengers = 100;
	var passengers_on_board = 0;
	var progress = 0f;
	var distance_to_destination = 1f;
	var moving = false;
	var current_city = spawn_point;
	var program : List[City] = Nil; //liste de villes à parcourir
	var cargo = scala.collection.immutable.Set[Request]() //maybe use another data structure ?

	var unlocked = false;

	def unlock_transport() =
		unlocked = true
	
	def lock_transport() : Boolean =
		val res = unlocked;
		unlocked = false;
		res
	
	def is_unlocked() : Boolean =
		unlocked

	def next_stop() =
		program match {
			case Nil =>
				moving = false;
				progress = 0f;
			case city :: _ => {
				printf("Transport going from " + current_city.name + " to " + city.name + "\n");
				moving = true;
				progress = 0f;
				val (xA, yA) = city.position;
				val (xB, yB) = current_city.position;
				//TODO : fancy courbe
				distance_to_destination = scala.math.sqrt((xA-xB)*(xA-xB) + (yA-yB)*(yA-yB)).toFloat;
				passengers_on_board = min(current_city.get_pop_waiting(), capacity_passengers);
				current_city.departure(passengers_on_board);
				capacity_passengers = capacity_passengers - passengers_on_board;
				current_city.departure(passengers_on_board);
			}
		}
	
	def position_moving() : (Float, Float) = 
		val (xA, yA) = current_city.position;
		val (xB, yB) = program.head.position;
		val t = progress/distance_to_destination;
		((xB - xA) * t + xA, (yB - yA) * t + yA)

	def rotation_moving() : Float =
		val (xA, yA) = current_city.position;
		val (xB, yB) = program.head.position;
		(scala.math.atan2(yA-yB, xA-xB) * 180f / 3.141592653589793).toFloat;

	def update(dt:Float) : Int =
		var benefit = 0;
		if (moving) {
			val destination = program.head;
			progress += vit * dt;
			if (progress >= distance_to_destination) {
				benefit = destination.arrival(passengers_on_board);
				capacity_passengers += passengers_on_board;
				passengers_on_board = 0;
				program match { //pop(program)
				case Nil => ()
				case new_current :: l =>
					current_city = new_current;
					program = l;
				}
				next_stop();
			}
			//update cargo
			for (q <- cargo)
				benefit += q.update(dt, Some(this))
				if(q.check_satisfied(Some(this)) || q.check_failed(Some(this))) then
					cargo -= q
		}
		benefit
	
	def begin_travel(road: List[City]) =
		program = road;
		current_city = program.head
		printf("Transport currently in station at " + (current_city.name) + "\n" )
		program = {
			program match {
				case Nil => printf("Transport, begin_travel : road should not have had a head\n"); Nil
				case _ :: next => next
			}
		}
		next_stop()
	
	def draw(window:RenderWindow) =
		if (moving) {
			sprite.origin = (sprite.localBounds.width/2, sprite.localBounds.height/2)
			sprite.position =  position_moving()
			sprite.rotation = rotation_moving()
			sprite.scale = (-spritescale, (if (sprite.rotation< 270 && sprite.rotation > 90) -1 else 1) * spritescale)
			window.draw(sprite)
			//draw cargo above the transport
			var offset = 0
			for (q <- cargo)
				q.draw(window, font, offset * 2, Some(this));
				offset = offset + 1
		}

	def mousePressed(b:Mouse.Button, mouseX:Int, mouseY:Int) = 
		true


class Train (name: String, vit: Float, spawn_point: City, sprite: Sprite, font:Font) extends Transport(name, vit, spawn_point, sprite, 0.07f, font)
class Plane (name: String, vit: Float, spawn_point: City, sprite: Sprite, font:Font) extends Transport(name, vit, spawn_point, sprite, 2.5f, font)
class Bus	(name: String, vit: Float, spawn_point: City, sprite: Sprite, font:Font) extends Transport(name, vit, spawn_point, sprite, 0.2f, font)
class Boat	(name: String, vit: Float, spawn_point: City, sprite: Sprite, font:Font) extends Transport(name, vit, spawn_point, sprite, 0.2f, font)