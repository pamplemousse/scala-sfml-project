import sfml.graphics.*
import sfml.window.*
import scala.collection.mutable.* //for HashMap

enum reward_type :
	case Money, Resources

//might be used for non-cash reward at some point in the future
trait Reward(val t: reward_type, val quantity: Int)
	//def cash_reward() : Unit

trait Request:
	var current_city:City
	def check_satisfied(transport_option : Option[Transport]) : Boolean
	def check_failed(transport_option : Option[Transport]) : Boolean
	//def cash_reward() : Unit
	def update(dt: Float, transport_option : Option[Transport]) : Int
	def draw (window:RenderWindow, font : Font, offset:Int, transport_option : Option[Transport]) : Unit

class PassengerRequest(val from: City, val sizeConstraint : Option[Int], val typeConstraint : Option[city_type], val reward: Int) extends SimpleShape with Reward(reward_type.Money, reward) with Request:
	var current_city = from;
	
	def city_satisfies(city : City) : Boolean =
		val res = 
		(sizeConstraint match {
			case None => true
			case Some(size) => (size == city.size) 
		})
		&& (typeConstraint match {
			case None => true
			case Some(ctype) =>  (ctype == city.ctype) 
		})
		res
		
	def is_satisfiable(worldMap: World) : Boolean = //checks if there is a city than can satisfy the constraints
		for i <- 0 to (worldMap.nodes.length -1) do 
			if (city_satisfies(worldMap.nodes(i)))
				return true
		return false
	
	def describes() =
		val s = "request with size" +
		(sizeConstraint match {
			case None => " none "
			case Some(size) => s" ${size} "
		}) + "and type" +
		(typeConstraint match {
			case None => " none "
			case Some(city_type.Circle) => " circle "
			case Some(city_type.Triangle) => " triangle "
			case Some(city_type.Square) => " square "
		}) + "at " + current_city.name
		println(s)
	
	def check_failed(transport_option : Option[Transport]) : Boolean = 
		//fails if the passenger took a transport which stopped whitout bringing them to the right city
		(! check_satisfied(transport_option)) &&
		(transport_option match {
			case None => false
			case Some(transport) => ! transport.moving
		})
	
	def check_satisfied(transport_option : Option[Transport]) =
		transport_option match {
			case None => ()
			case Some(transport) => current_city = transport.current_city
		}
		city_satisfies(current_city)
	def update(dt: Float, transport_option : Option[Transport]) =
		if(check_satisfied(transport_option)) then ()
			// TODO : signal win (reward appearing)
		else if (check_failed(transport_option)) then ()
			// TODO : signal fail (constraint not satisfied)
		else ()
		if check_satisfied(transport_option) then {
			//println(s"won ${reward} by transporting a passenger !") 
			reward
		}
		else 0
		
	def drawTypeContraint (window:RenderWindow, font : Font, pos: (Float, Float)) = 
		typeConstraint match {
				case None => 
					draw_circle(window, sfml.graphics.Color.Transparent(), pos, 1);
					draw_triangle(window, sfml.graphics.Color.Transparent(), pos, 1);
					draw_square(window, sfml.graphics.Color.Transparent(), pos, 1);
				case Some(ctype) => 
					ctype match {
						case city_type.Circle => draw_circle(window, sfml.graphics.Color(0xEF, 0x78, 0x07), pos, 1);
						case city_type.Triangle => draw_triangle(window, sfml.graphics.Color(0xEF, 0x78, 0x07), pos, 1);
						case city_type.Square => draw_square(window, sfml.graphics.Color(0xEF, 0x78, 0x07), pos, 1);
					}
			}
		
	def drawSizeContraint (window:RenderWindow, font : Font, pos: (Float, Float)) = 
		sizeConstraint match {
			case None => ()
			case Some(size) => 
			val tf = Text(s"${size}", font, 30)
			tf.fillColor = sfml.graphics.Color(255, 255, 0);
			tf.outlineColor = sfml.graphics.Color(0, 0, 0);
			tf.outlineThickness = 3
			tf.origin = (tf.localBounds.left + tf.localBounds.width/2, tf.localBounds.top + tf.localBounds.height/2)
			tf.position = pos
			window.draw(tf)
		}

	def draw (window:RenderWindow, font : Font, offset:Int, transport_option : Option[Transport]) = 
		transport_option match {
			case None => 
				val (x, y) = current_city.position
				val pos = (x, (y - sizeFactor * (offset + current_city.size+1)).toFloat)
				drawTypeContraint(window, font, pos)
				drawSizeContraint(window, font, pos)
			case Some(transport) =>
				val (x, y) = transport.position_moving()
				val pos = (x, (y - sizeFactor * (offset + current_city.size+1)).toFloat)
				drawTypeContraint(window, font, pos)
				drawSizeContraint(window, font, pos)
		}
	


class RequestHandler(val worldMap: World, val font: Font) :

	var requests = scala.collection.immutable.Set[Request]() //maybe use another data structure ?
	//TODO : store requests indexed by cities for efficiency + more elegant code
	val rng = new scala.util.Random
		
	def generate_passenger_request() =
		var success = false
		while (!success) do
			val constraint_class = rng.nextInt(3) 
			// 0 : both type and size, 1 : only size, 2 : only type
			// might be changed in the future to add a better probability distribution
			val typeConstraint = (
				if(constraint_class != 1) then
					(rng.nextInt(3)) match {
						case 0 => Some(city_type.Circle)
						case 1 => Some(city_type.Triangle)
						case 2 => Some(city_type.Square)
					}
				else
					None		
			)
			val sizeConstraint = (
				if(constraint_class != 2) then
					Some (1+rng.nextInt(maxCitySize)) //in [1..maxCitySize]
				else
					None		
			)
			//TODO : check if a city on the map can satisfy those constraints (and restart the process otherwise)
			val from_city = rng.nextInt(worldMap.nodes.length)
			val reward = rng.nextInt(200) //TODO : tune this, for example as a function of contraints
			val newrequest = PassengerRequest(worldMap.nodes(from_city), sizeConstraint, typeConstraint, reward);
			if (newrequest.is_satisfiable(worldMap)) then
				//if the request can not be fulfilled : as (success == false), we try again
				while newrequest.city_satisfies(newrequest.current_city) do 
					newrequest.current_city = worldMap.nodes(rng.nextInt(worldMap.nodes.length))
				//println("passenger request created")
				requests = requests incl newrequest //add the request
				success = true

	def generate_resource_request() =
		var success = false
		var from_city = 0
		while (!success) do
			from_city = rng.nextInt(worldMap.nodes.length)
			if (worldMap.nodes(from_city).resources_production.length != 0)
				success = true
		val reward = rng.nextInt(200) //TODO : tune this, for example as a function of contraints
		val rank = rng.nextInt(worldMap.nodes(from_city).resources_production.length)
		val res = worldMap.nodes(from_city).resources_production.slice(rank, rank+1).head
		val newrequest = ResourcesRequest(worldMap.nodes(from_city), res, reward);
		requests = requests incl newrequest //add the request

	def update(dt:Float) : Int =
		var benefit = 0;
		for (q <- requests)
			benefit += q.update(dt, None);
			if(q.check_satisfied(None) || q.check_failed(None)) then
				requests -= q //remove the request
		//TODO : fine tune adding more requests, as with a timer
		//also add an "expiration" timer to requests
		if (requests.size < 30) then //high value here for debugging purposes
			(rng.nextInt(2)) match {
					case 0 => generate_passenger_request()
					case 1 => generate_resource_request()
				}
		benefit
	
	def transport_launched(transport:Transport) = 
		val start_city = transport.current_city
		for (q <- requests)
			if ((q.current_city == start_city)) then
				//transfer the request as cargo to the transport
				requests -= q
				transport.cargo = transport.cargo incl q
			
		 //TODO
	
	def draw (window:RenderWindow) = 
		var request_per_city : HashMap[City, Int] = HashMap[City, Int]()
		for i <- 0 to (worldMap.nodes.length -1) do 
			request_per_city.update(worldMap.nodes(i), 0)
		for (q <- requests)
			//the hash map and this offset allow to draw multiple request in one city whitout issue
			var offset = request_per_city.getOrElse(q.current_city, 0)
			request_per_city.update(q.current_city, offset + 1)
			q.draw(window, font, offset * 2, None);
		

