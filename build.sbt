enablePlugins(Documentation, Linter, SFML)

name := "Turbo-Projet-Scala"
version := "0.0.0.1.0"

buildMode := BuildMode.Debug

libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "2.32.0"